package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.junit.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;


//comment the above line and uncomment below line to use Chrome
//import org.openqa.selenium.chrome.ChromeDriver;
@Component
public class SelTest implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception{
        //System.setProperty("webdriver.gecko.driver", "C:\\Users\\d.larocca\\Desktop\\kubectl\\geckodriver.exe");
        System.setProperty("webdriver.gecko.driver","/var/geckodriver");
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("-headless");

        
        WebDriver driver = new FirefoxDriver(options);

        String baseUrl = "http://10.39.10.33:8081";

        driver.get(baseUrl);

        WebElement footer = null;
        
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        try{
            footer = driver.findElement(By.id("block_various_links_footer"));
        }catch(Exception e){
            System.out.println("footer not found");
            driver.quit();
            return;
        }

        WebElement aboutUs = null;

        List<WebElement> elements = null;

        try{
            elements = footer.findElements(By.tagName("li"));
        }catch(Exception e) {
            System.out.println("footer elements not found");
            driver.quit();
            return;
        }
        for(WebElement el : elements){
            if(el.getText().equals("About us")) {
                aboutUs = el;
                break;
            }
        }
        //Thread.sleep(10);
        if(aboutUs != null){
            System.out.println("Test Passed - About Us found");
            aboutUs = aboutUs.findElement(By.tagName("a"));
            aboutUs.click();
        }else{
            System.out.println("Test Failed - About Us not found");
        }
        
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        try {
            WebElement checkPage = driver.findElement(By.xpath("//div[@class='alert alert-danger']"));
            System.out.println("Test Failed - page not found");
            Assert.assertEquals("bla","nope");
        }catch(Exception e){
            Assert.assertEquals("bla","bla");
            System.out.println("Test Passed - page opened");
        }

        //Thread.sleep(10000);

        driver.quit();

    }
}
